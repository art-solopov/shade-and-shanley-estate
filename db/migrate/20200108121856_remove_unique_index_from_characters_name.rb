class RemoveUniqueIndexFromCharactersName < ActiveRecord::Migration[5.2]
  def up
    remove_index :characters, name: :index_characters_on_lower_name
  end

  def down
    add_index :characters, '(lower(name))', unique: true
  end
end
