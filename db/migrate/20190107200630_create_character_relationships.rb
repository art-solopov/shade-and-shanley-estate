class CreateCharacterRelationships < ActiveRecord::Migration[5.2]
  def change
    create_table :character_relationships do |t|
      t.belongs_to :character_left, foreign_key: { to_table: :characters }
      t.string :label_left
      t.belongs_to :character_right, foreign_key: { to_table: :characters }
      t.string :label_right

      t.timestamps
    end
  end
end
