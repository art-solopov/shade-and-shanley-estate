class CreatePermanentAuthTokens < ActiveRecord::Migration[5.2]
  def change
    create_table :permanent_auth_tokens do |t|
      t.string :token, null: false
      t.references :user, foreign_key: true
      t.timestamp :expires_at, null: false

      t.timestamps

      t.index [:token, :expires_at]
    end
  end
end
