class CreateCharacters < ActiveRecord::Migration[5.1]
  def change
    create_table :characters do |t|
      t.belongs_to :author, foreign_key: { to_table: :users }
      t.string :name, limit: 1023
      t.string :species, limit: 1023
      t.text :bio

      t.timestamps
    end
  end
end
