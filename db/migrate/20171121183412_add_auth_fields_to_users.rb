class AddAuthFieldsToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :uid, :string
    add_column :users, :last_login, :timestamp
    add_column :users, :invite_code, :string
    add_column :users, :user_type, :string
    add_column :users, :blocked, :boolean, default: false

    add_index :users, :uid
    add_index :users, :invite_code
    add_index :users, :blocked
  end
end
