class AddPictureToCharacters < ActiveRecord::Migration[5.1]
  def change
    change_table :characters do |t|
      t.column :picture_url, :string, limit: 2048
      t.column :picture_source_url, :string, limit: 2048
    end
  end
end
