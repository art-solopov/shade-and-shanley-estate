class AddHiddenToCharacters < ActiveRecord::Migration[5.2]
  def change
    add_column :characters, :hidden, :boolean, default: false
    add_index :characters, :hidden
  end
end
