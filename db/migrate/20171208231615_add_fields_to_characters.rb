class AddFieldsToCharacters < ActiveRecord::Migration[5.1]
  def change
    add_column :characters, :athenaeum_character, :boolean, default: false
    add_column :characters, :role, :string

    add_index :characters, :athenaeum_character
    add_index :characters, :role
  end
end
