class AddNameIndexToCharacters < ActiveRecord::Migration[5.1]
  def change
    add_index :characters, '(lower(name))', unique: true
  end
end
