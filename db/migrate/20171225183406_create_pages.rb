class CreatePages < ActiveRecord::Migration[5.1]
  def change
    create_table :pages do |t|
      t.string :type
      t.text :content

      t.timestamps

      t.index :type
    end
  end
end
