class AddTrigramsToCharacters < ActiveRecord::Migration[5.2]
  def change
    enable_extension :pg_trgm
    add_index :characters, :name, using: 'gist', opclass: :gist_trgm_ops
  end
end
