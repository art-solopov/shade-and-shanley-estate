module OmniauthHelper
  def sign_in_path(provider: Settings.omniauth.provider)
    "/auth/#{provider}"
  end

  def invite_url(invite_code, provider: Settings.omniauth.provider)
    uri = URI(root_url).tap do |u|
      u.path = sign_in_path(provider: provider)
      u.query = URI.encode_www_form(invite_code: invite_code)
    end
    uri.to_s
  end
end
