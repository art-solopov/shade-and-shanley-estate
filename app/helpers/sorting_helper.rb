module SortingHelper
  SORT_ICONS = {
    asc: 'triangle-up',
    desc: 'triangle-down'
  }

  def sort_link(content = nil, sort_column)
    current_sort_column, current_sort_order = @sort_column, @sort_order
    is_current_sort = current_sort_column == sort_column.to_s

    icon = is_current_sort ? octicon(SORT_ICONS[current_sort_order.to_sym], class: 'ml-1') : ''

    sort_order = if is_current_sort
                   current_sort_order == 'asc' ? :desc : :asc
                 else 'asc'
                 end

    content ||= capture { yield }
    content = content_tag(:span, content) + icon
    link_to content, { sort_column: sort_column, sort_order: sort_order }, class: 'sorting-link'
  end
end
