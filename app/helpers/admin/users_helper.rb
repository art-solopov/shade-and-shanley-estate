module Admin::UsersHelper
  def invite_code_button(user)
    return if user.invite_code.blank?

    button_tag(type: :button, 
               class: 'btn btn-secondary btn-sm invite-link', data: {
                 invite_url: invite_url(user.invite_code),
                 user_name: user.display_name,
                 action: 'user-invite-links#openModal'
               }) { octicon(:info, height: 24) }
  end
end
