# frozen_string_literal: true

module AdminHelper
  # TODO: refactor into helper modules

  def admin_paginator(collection)
    return if collection.total_pages <= 1

    content_tag(:div, class: %i[row mb-2]) do
      content_tag(:div, class: %i[col-sm-12]) do
        paginate(collection, views_prefix: 'admin')
      end
    end
  end
end
