module BootstrapHelper
  def form_group
    content_tag(:div) { yield }
  end

  def bootstrap_form_for(object, **options)
    form_for(object, builder: Bootstrap::FormBuilder, **options) { |f| yield f }
  end

  FLASH_TYPES = {
    notice: :success,
    alert: :danger
  }

  def bootstrap_alert(type, text)
    content = content_tag(:span, text) + close_button
    content_tag(:div, content, class: "alert alert-#{FLASH_TYPES[type]}",
                role: :alert)
  end

  def close_button
    content = content_tag(:span, '×', aria: { hidden: true })
    button_tag(content, type: :button, class: :close,
               data: { dismiss: :alert }, aria: { label: :close })
  end
end
