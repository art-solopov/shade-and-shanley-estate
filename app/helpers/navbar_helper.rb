module NavbarHelper
  def navbar_link(text, path, color: nil, **options)
    cls = ['nav-link'].tap do |a|
      a << "text-#{color}" if color
    end
    navbar_item { link_to text, path, class: cls, **options }
  end

  def navbar_text(text, color: nil)
    cls = ['navbar-text'].tap do |a|
      a << "text-#{color}" if color
    end
    navbar_item { content_tag(:span, text, class: cls) }
  end

  def navbar_item
    content_tag(:li, yield, class: 'nav-item')
  end
end
