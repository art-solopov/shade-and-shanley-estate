# frozen_string_literals: true

module Front::CharactersHelper
  def characters_group_vals
    content = @characters.keys.map(&method(:characters_group_item))
    content_tag(:ul, content.join("\n").html_safe)
  end

  def characters_group_item(group_val)
    content_tag(:a, group_val.title, href: "##{group_val.key}", class: 'button is-text')
  end

  def character_relationship_delete_btn(object)
    action = object.persisted? ? :disable : :destroy
    button_tag 'Delete', type: :button,
               class: 'button is-danger is-fullwidth',
               data: { action: "character-relationships-form##{action}" }
  end
end
