module ApplicationHelper
  def enumerize_select_options(attr)
    attr.values.map do |e|
      [I18n.t(e, scope: attr.i18n_scopes), e]
    end
  end
end
