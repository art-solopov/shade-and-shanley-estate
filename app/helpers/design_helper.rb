module DesignHelper
  def octicon(name, **options)
    Octicons::Octicon.new(name, **options).to_svg.html_safe
  end

  def edit_link(path, title: 'Edit', **opts)
    cls = Array(opts[:class]).prepend('btn btn-primary').join(' ')
    link_to(path, class: cls) { octicon(:pencil) + ' ' + content_tag(:span, title) }
  end

  def delete_link(path, title: 'Delete', confirm: 'Are you sure?', **opts)
    cls = Array(opts[:class]).prepend('btn btn-danger').join(' ')
    link_to(path, class: cls, method: :delete, data: { confirm: confirm }) do
      octicon(:trashcan) + ' ' + content_tag(:span, title)
    end
  end
end
