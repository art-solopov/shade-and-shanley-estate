class CharactersSearchForm
  include ActiveModel::Model
  include ActiveModel::Attributes

  attribute :search, :string
  attribute :author_id, :integer
end
