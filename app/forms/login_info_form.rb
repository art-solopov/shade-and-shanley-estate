class LoginInfoForm
  include ActiveModel::Model
  include ActiveModel::Attributes

  attribute :remember_me, :boolean
end
