# frozen_string_literal: true

class CharactersIndex::CharacterDecorator < Draper::Decorator
  delegate_all

  TOKEN_CLASSES = {
    own: 'is-success',
    athenaeum: 'is-info'
  }.freeze

  def item_classes
    [
      'character-index-item',
      ('is-own-character' if author == h.current_user),
      ('is-hidden-character' if hidden?)
    ].compact
  end

  def actions
    %i[edit destroy]
      .select { |e| h.policy(object).public_send(:"#{e}?") }
  end

  def links
    {
      edit: h.link_to('Edit', h.edit_character_path(id), class: %w[button is-link]),
      destroy: h.link_to('Delete', h.character_path(id),
                         class: %w[button is-danger],
                         method: :delete,
                         data: { confirm: 'Are you sure?' })
    }
  end

  def token_flags
    [
      ('own' if author == h.current_user),
      ('athenaeum' if athenaeum_character?)
    ].compact
  end

  def tokens
    token_flags.map do |token|
      h.content_tag(:span, token.first.capitalize,
                    class: ['character-index-item__token',
                          "character-index-item__token--#{token}",
                          'tag',
                          TOKEN_CLASSES[token.to_sym]],
                    title: h.t(token, scope: 'front.characters.index.tokens'))
    end.join(' ')
  end

  def character_image_link
    h.link_to(h.character_path(object), target: '_blank') { character_image }
  end

  def character_image
    if object.picture_url.present?
      h.content_tag(
        :div,
        h.image_tag(object.picture_url, class: 'character-index-item__img'),
        class: 'character-index-item__img-container'
      )
    else
      h.image_tag(h.image_path('no_img.png'),
                  class: 'character-index-item__img')
    end
  end
end
