class Admin::UserDecorator < Draper::Decorator
  delegate_all

  def show_fields
    return enum_for(__method__) unless block_given?

    %i[display_name user_type last_login].each do |field|
      yield [User.human_attribute_name(field), public_send(field)]
    end

    if object.invite_code.present?
      yield ['Invite url', h.invite_url(object.invite_code)]
    end

    self
  end

  def last_login
    return '—' unless object.last_login

    I18n.l(object.last_login)
  end

  def user_type
    object.user_type.text
  end
end
