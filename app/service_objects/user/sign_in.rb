# frozen_string_literal: true

class User
  class SignIn
    NOT_FOUND_STATUS = :not_found

    def initialize(auth, params)
      @uid = auth['uid']
      @login_info = LoginInfoForm.new(params['login_info_form'])
    end

    attr_reader :user, :permanent_token

    def call
      User.transaction do
        _call
      end
    end

    private

    def _call
      fetch_user
      return self.class::NOT_FOUND_STATUS unless @user

      @permanent_token = PermanentAuthToken.generate!(@user) if @login_info.remember_me

      @user.update(new_data)
      :ok
    end

    def new_data
      { last_login: Time.zone.now }
    end

    def fetch_user
      @user = scope.find_by(uid: @uid)
    end

    def scope
      User.unblocked
    end
  end
end
