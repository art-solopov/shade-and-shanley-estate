# frozen_string_literal: true

class User
  class CheckPermanentToken
    def initialize(token)
      @token = token
    end

    def call
      PermanentAuthToken.active.find_by(token: @token)
    end
  end
end
