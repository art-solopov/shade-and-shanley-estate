class User
  class Register < SignIn
    NOT_FOUND_STATUS = :not_invited

    def initialize(auth, params)
      super
      @invite_code = params['invite_code']
    end

    private

    def fetch_user
      @user = scope.find_by(invite_code: @invite_code)
    end

    def new_data
      super.merge(uid: @uid, invite_code: nil)
    end
  end
end
