class User
  class GenerateInviteCode
    def call
      code = nil
      while code.blank? || User.where(invite_code: code).select(1).present?
        code = SecureRandom.hex(12)
      end
      code
    end
  end
end
