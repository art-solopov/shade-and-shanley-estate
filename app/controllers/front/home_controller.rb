module Front
  class HomeController < BaseController
    skip_before_action :ensure_login

    def show
      # TODO: move to a container
      @front_page = Pages::FrontPage.first
      fresh_when [current_user, @front_page] unless Rails.env.development?
    end
  end
end
