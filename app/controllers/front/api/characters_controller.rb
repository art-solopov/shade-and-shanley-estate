module Front
  module Api
    class CharactersController < ApplicationController
      def search
        @use_case = CharactersSearch.call(user: current_user,
                                          search: params[:search])
        render json: @use_case.collection
      end
    end
  end
end