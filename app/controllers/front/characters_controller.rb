module Front
  class CharactersController < BaseController
    helper_method :search_params

    def index
      authorize Character
      @search_form = CharactersSearchForm.new(search_params)
      result = CharactersIndex.(user: current_user, search_form: @search_form, page: params[:page])
      @characters = result.collection
      @authors = User.joins(:characters).order(:display_name).distinct
    end

    def show
      @character = find_character
      fresh_when @character
    end

    def new
      @character = build_character
      authorize @character
    end

    def create
      @character = build_character
      authorize @character
      save_character || render(:new)
    end

    def edit
      @character = find_character
    end

    def update
      @character = find_character
      save_character || render(:edit)
    end

    def destroy
      @character = find_character
      @character.destroy
      redirect_to characters_path, notice: 'Character deleted'
    end

    private

    def find_character
      Character.visible_for(current_user)
               .find(params[:id])
               .tap { |ch| authorize(ch) }
    end

    def build_character
      Character.new(author: current_user)
    end

    def save_character
      @character.assign_attributes(character_params)
      if @character.save
        redirect_to character_path(@character), notice: t('.success')
      else
        Rails.logger.debug @character.errors.full_messages
        false
      end
    end

    def character_params
      params.require(:character)
            .permit(:name, :species, :bio, :role,
                    :athenaeum_character, :hidden,
                    :picture_url, :picture_source_url,
                    left_character_relationships_attributes: [
                      :_destroy, :id, :character_right_id,
                      :label_left, :label_right
                    ],
                    right_character_relationships_attributes: [
                      :_destroy, :id, :character_left_id,
                      :label_left, :label_right
                    ]
                   )
    end

    def search_params
      return {} unless params[:q]

      params[:q].permit(:search, :author_id)
    end

    def title
      case action_name
      when 'index' then "#{super} — characters list"
      when 'new', 'create' then "#{super} — new character"
      when 'edit', 'update' then "#{super} — editing character #{@character.name}"
      else super
      end
    end
  end
end
