class SessionsController < ApplicationController
  skip_before_action :ensure_login
  skip_before_action :verify_authenticity_token, only: :create
  layout 'front'

  def new
    render :login
  end

  def create
    @service = if omniauth_params['invite_code']
                 User::Register.new(omniauth_auth, omniauth_params)
               else
                 User::SignIn.new(omniauth_auth, omniauth_params)
               end

    status = @service.call
    case status
    when :ok
      @user = @service.user
      session[:user_id] = @user.id
      if (token = @service.permanent_token)
        cookies.signed[PERMANENT_TOKEN_COOKIE_NAME] = {
          value: token.token,
          expires: token.expires_at,
          httponly: true
        }
      end
    when :not_found then flash[:alert] = 'Login failed'
    when :not_invited then return render :not_invited
    end

    redirect_to root_path
  end

  def destroy
    if session[:user_id].nil?
      return redirect_to(root_path, alert: 'Not logged in')
    end

    session[:user_id] = nil
    if permanent_token
      token_model = PermanentAuthToken.active.find_by(token: permanent_token)
      token_model&.update!(expires_at: Time.current)
    end
    redirect_to root_path, notice: 'Logged out successfully'
  end

  private

  def omniauth_auth
    request.env['omniauth.auth'].to_h
  end

  def omniauth_params
    request.env['omniauth.params'].to_h
  end
end
