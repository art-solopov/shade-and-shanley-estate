class ApplicationController < ActionController::Base
  include Pundit
  PERMANENT_TOKEN_COOKIE_NAME = :remember_me

  protect_from_forgery with: :exception

  helper_method :current_user
  before_action :check_permanent_token
  before_action :ensure_login

  helper_method :title

  private

  def check_permanent_token
    return unless permanent_token

    token_model = User::CheckPermanentToken.new(permanent_token).call
    if token_model
      session[:user_id] = token_model.user_id
    else
      cookies.delete(PERMANENT_TOKEN_COOKIE_NAME)
    end
  end

  def ensure_login
    return if current_user

    redirect_to(login_path,
                alert: 'Please login before accessing the page')
  end

  def permanent_token
    @_permanent_token ||= cookies.signed[PERMANENT_TOKEN_COOKIE_NAME]
  end

  def current_user
    return nil unless session[:user_id]

    @_current_user ||= User.find(session[:user_id])
  end

  def interactor(klass, opts = {})
    klass.(opts.merge(user: current_user))
  end

  def title
    'The Shade Estate'
  end
end
