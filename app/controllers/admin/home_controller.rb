# frozen_string_literal: true

module Admin
  class HomeController < BaseController
    def show
      @links = [
        ['Characters', admin_characters_path],
        ['Users', admin_users_path],
        ['Edit front page', admin_pages_front_page_path]
      ]
    end
  end
end
