module Admin
  class CharactersController < BaseController
    include WithSorting

    SORT_COLUMNS = {
      id: :_self,
      athenaeum_character: :_self,
      author_name: 'users.display_name',
      name: :_self,
      species: :_self,
      role: :_self
    }

    def index
      @characters = Character.includes(:author)
                             .references(:author)
                             .order(custom_order)
                             .page(params[:page])
                             .per(20)
    end

    def edit
      @character = find_character
    end

    def new
      @character = build_character
    end

    def create
      @character = build_character
      save_character || render(:new)
    end

    def update
      @character = find_character
      save_character || render(:edit)
    end

    def destroy
      @character = find_character
      @character.destroy
      redirect_to admin_characters_path, notice: 'Character deleted'
    end

    private

    def find_character
      Character.find(params[:id])
    end

    def build_character
      Character.new(author: current_user)
    end

    def save_character
      if @character.update(character_params)
        redirect_to admin_characters_path, notice: t('.success')
      end
    end

    def character_params
      params.require(:character).permit(:name, :species, :bio, :role,
                                        :author_id, :athenaeum_character,
                                        :hidden, :picture_url, :picture_source_url)
    end
  end
end
