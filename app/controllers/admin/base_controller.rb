module Admin
  class BaseController < ApplicationController
    layout 'admin'

    before_action :ensure_admin

    private

    def ensure_admin
      authorize :admin, :access?
    end

    def title
      'The Shade Estate — admin interface'
    end
  end
end
