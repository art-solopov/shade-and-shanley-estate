module Admin
  module Pages
    class FrontPagesController < ::Admin::BaseController
      PAGE_CLASS = ::Pages::FrontPage

      def show
        @page = get_page
      end

      def edit
        @page = get_page
      end

      def update
        @page = get_page.tap { |pg| pg.attributes = page_params }
        if @page.save
          redirect_to({ action: :show }, notice: "Front page saved")
        else
          render :edit
        end
      end

      private

      def get_page
        # TODO: move to the container
        PAGE_CLASS.first || PAGE_CLASS.new(content: '')
      end

      def page_params
        params.require(:page).permit(:content)
      end
    end
  end
end
