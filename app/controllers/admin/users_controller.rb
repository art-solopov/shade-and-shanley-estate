class Admin::UsersController < Admin::BaseController
  include WithSorting

  SORT_COLUMNS = {
    id: :_self,
    display_name: :_self,
    last_login: :_self,
    blocked: :_self
  }.freeze

  def index
    @users = User.order(custom_order)
                 .page(params[:page])
                 .per(20)
  end

  def show
    @user = Admin::UserDecorator.decorate find_user
  end

  def new
    @user = User.new
  end

  def edit
    @user = find_user
  end

  def update
    @user = find_user
    if @user.update(user_params)
      redirect_to admin_users_path, notice: 'User updated successfully'
    else
      render :edit
    end
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to admin_users_path, notice: 'User created successfully'
    else
      render :new
    end
  end

  def destroy
    @user = find_user
    @user.destroy
    redirect_to admin_users_path, notice: 'User deleted'
  end

  def block
    @user = find_user
    @user.update(blocked: true)
    redirect_to admin_users_path, notice: 'User blocked'
  end

  def unblock
    @user = find_user
    @user.update(blocked: false)
    redirect_to admin_users_path, notice: 'User unblocked'
  end

  def invite_code
    so = User::GenerateInviteCode.new
    render json: { invite_code: so.call }
  end

  private

  def find_user
    User.find(params[:id])
  end

  def user_params
    params.require(:user)
          .permit(:display_name, :user_type, :invite_code, :blocked)
  end
end
