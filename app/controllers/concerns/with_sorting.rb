module WithSorting
  extend ActiveSupport::Concern

  included do
    helper_method :custom_order

    before_action :assign_custom_order
  end

  DEFAULT_SORT_COLUMN = 'id'.freeze
  DEFAULT_SORT_ORDER = 'asc'.freeze

  private

  def assign_custom_order
    ordering_params = params.permit(:sort_column, :sort_order)
                            .reject { |_, v| v.blank? }

    @sort_column = ordering_params['sort_column'] || DEFAULT_SORT_COLUMN
    @sort_order = ordering_params['sort_order'] || DEFAULT_SORT_ORDER

    @sort_order = DEFAULT_SORT_ORDER unless @sort_order.in?(%w[asc desc])
  end

  def custom_order
    sort_column = case (col = self.class::SORT_COLUMNS[@sort_column.to_sym])
                  when :_self then @sort_column
                  when String then col.to_s
                  else DEFAULT_SORT_COLUMN
                  end

    if sort_column.include?('.')
      "#{sort_column} #{@sort_order.upcase}"
    else
      { sort_column => @sort_order }
    end
  end
end
