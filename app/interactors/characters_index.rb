# frozen_string_literal: true

class CharactersIndex
  include Interactor

  PER_PAGE = 20

  def call
    @search_form = context.search_form
    @scope = Pundit.policy_scope!(context.user, Character)
                   .includes(:author)
                   .page(context.page)
                   .per(PER_PAGE)
                   .default_order
    filter_scope!
    decorate_collection!
  end

  private

  def filter_scope!
    if @search_form.search.present?
      @scope = @scope.where('? <% name', @search_form.search)
    end

    if @search_form.author_id
      @scope = @scope.where(author_id: @search_form.author_id)
    end

    @scope
  end

  def decorate_collection!
    context.collection = CharactersIndex::CharacterDecorator.decorate_collection(@scope)
  end
end
