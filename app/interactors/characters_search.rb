class CharactersSearch
  include Interactor

  LIMIT = 15

  def call
    return if context.search.blank?
    scope = Pundit.policy_scope!(context.user, Character)
              .where('name ILIKE ?', "#{context.search}%")
              .order(:name)
    context.collection = scope.limit(LIMIT).map { |e| [e.id, e.name] }
  end
end
