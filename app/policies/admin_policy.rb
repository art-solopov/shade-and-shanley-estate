class AdminPolicy
  attr_reader :user

  def initialize(user, _record)
    @user = user
  end

  def access?
    user.user_type == 'admin'
  end
end
