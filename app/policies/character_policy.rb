class CharacterPolicy < ApplicationPolicy
  def create?
    record.author == user
  end

  def update?
    record.author == user
  end

  def destroy?
    update?
  end

  def set_athenaeum?
    user.user_type != 'estate_user'
  end

  class Scope < Scope
    def resolve
      subscope = scope.visible_for(user)
      if user.user_type == 'estate_user'
        subscope.estate_characters
      else
        subscope
      end
    end
  end
end
