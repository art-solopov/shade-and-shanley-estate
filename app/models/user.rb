class User < ApplicationRecord
  extend Enumerize

  has_many :characters, inverse_of: :author, foreign_key: :author_id

  validates :display_name, presence: true
  validates :invite_code, uniqueness: true,
            if: proc { |user| user.invite_code.present? }
  validates :uid, uniqueness: true, if: proc { |user| user.uid.present? }

  scope :unblocked, -> { where(blocked: false) }

  enumerize :user_type, in: %i[estate_user advanced_user admin],
            default: :estate_user
end
