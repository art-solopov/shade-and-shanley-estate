class PermanentAuthToken < ApplicationRecord
  VALIDITY_PERIOD = 2.weeks

  belongs_to :user

  validates :token, :expires_at, presence: true

  scope :active, -> { where('expires_at > ?', Time.current) }

  def self.generate!(user)
    create!(
      user: user,
      token: SecureRandom.base58(12),
      expires_at: Time.current + VALIDITY_PERIOD
    )
  end
end
