class CharacterRelationship < ApplicationRecord
  belongs_to :character_left, class_name: 'Character'
  belongs_to :character_right, class_name: 'Character'

  validates :label_left, :label_right, presence: true
  validate :should_not_refer_to_self

  def self.for_character(character)
    where('character_left_id = :id OR character_right_id = :id',
          id: character.id).includes(:character_left, :character_right)
  end

  def self.right_visible
    joins(
      <<~SQL
        INNER JOIN characters chr
        ON chr.id = #{table_name}.character_right_id AND NOT(chr.hidden)
      SQL
    )
  end

  def self.left_visible
    joins(
      <<~SQL
        INNER JOIN characters chl
        ON chl.id = #{table_name}.character_left_id
        AND NOT(chl.hidden)
      SQL
    )
  end

  private

  def should_not_refer_to_self
    return if character_left != character_right
    errors.add(:base, :referencing_self)
  end
end
