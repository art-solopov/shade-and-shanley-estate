# frozen_string_literal: true

class Character < ApplicationRecord
  belongs_to :author, class_name: 'User'
  has_many :left_character_relationships, -> { right_visible },
           class_name: 'CharacterRelationship',
           foreign_key: :character_left_id,
           inverse_of: :character_left,
           dependent: :destroy
  has_many :right_character_relationships, -> { left_visible },
           class_name: 'CharacterRelationship',
           foreign_key: :character_right_id,
           inverse_of: :character_right,
           dependent: :destroy

  accepts_nested_attributes_for :left_character_relationships,
                                :right_character_relationships,
                                allow_destroy: true

  validates :author, :name, :species, :role, presence: true

  scope :default_order, -> { order(:name) }
  scope :estate_characters, -> { where(athenaeum_character: false) }
  scope :visible, -> { where('NOT hidden') }

  def self.visible_for(user)
    visible.or(where(author: user))
  end

  before_save :capitalize_indexable_fields

  def author_name
    author&.display_name
  end

  def relationships
    left = left_character_relationships.includes(:character_right).map do |rel|
      [rel.label_right, rel.character_right]
    end

    right = right_character_relationships.includes(:character_left).map do |rel|
      [rel.label_left, rel.character_left]
    end

    (left + right).sort_by(&:first)
  end

  private

  def capitalize_indexable_fields
    self.species = species.mb_chars.tap { |c| c[0] = c[0].upcase }.to_s
    self.role = role.mb_chars.tap { |c| c[0] = c[0].upcase }.to_s
  end
end
