import { Application } from 'stimulus'
import { definitionsFromContext } from 'stimulus/webpack-helpers'

const stmApp = Application.start()
const stmContext = require.context('../admin/components/stimulus_controllers', true, /\.js$/)
stmApp.load(definitionsFromContext(stmContext))
