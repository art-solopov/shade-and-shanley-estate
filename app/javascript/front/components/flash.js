function onFlashCloseBtnClick({target}) {
    let flash = target.parentElement
    flash.parentElement.removeChild(flash)
}

export default function addFlashClose() {
    let flashButtons = document.querySelectorAll('.flash-notification > .delete')
    flashButtons.forEach(el => el.addEventListener('click', onFlashCloseBtnClick))
}