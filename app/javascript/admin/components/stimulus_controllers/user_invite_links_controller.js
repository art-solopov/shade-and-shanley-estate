import { Controller } from 'stimulus'

export default class UserInviteLinksController extends Controller {
    openModal(event) {
        let target = event.target.closest('button')
        let { userName, inviteUrl } = target.dataset

        this.modalTitleTarget.innerText = `Invite link for ${userName}`
        this.modalBodyTarget.innerText = inviteUrl
        $(this.modalTarget).modal('show')
    }
}

UserInviteLinksController.targets = ['modal', 'modalTitle', 'modalBody']
