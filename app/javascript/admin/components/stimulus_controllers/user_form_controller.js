import { Controller } from 'stimulus'
import axios from 'axios'

export default class UserFormController extends Controller {
    generateInviteCode(event) {
        event.preventDefault()
        axios.get(event.target.href).then(res => {
            this.inviteCodeInputTarget.value = res.data.invite_code
        })
    }
}

UserFormController.targets = ['inviteCodeInput']
