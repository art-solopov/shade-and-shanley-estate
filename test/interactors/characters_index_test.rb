# frozen_string_literal: true

require 'test_helper'

class CharactersIndexTest < ActiveSupport::TestCase
  def setup
    @users = create_list(:user, 4)
    @search_form_mock = OpenStruct.new
  end

  test 'no filtering - default order by name' do
    @characters = gen_random_characters
    result = @characters.sort_by(&:name)
    _(subject.collection.count).must_equal result.count
    _(subject.collection.map(&:object)).must_equal result
  end

  test 'filtering by name' do
    @characters = gen_named_characters
    @search_form_mock.search = 'mike'
    result = @characters.select { |ch| ch.name.split(' ').first.in? %w[Mike Mikki] }
                        .sort_by(&:name)
    _(subject.collection.count).must_equal @users.count * 2
    _(subject.collection.map(&:object)).must_equal result
  end

  test 'filtering by address' do
    @characters = gen_random_characters
    @search_form_mock.author_id = @users.first
    result = @characters.select { |ch| ch.author_id == @users.first.id }
                        .sort_by(&:name)
    _(subject.collection.count).must_equal result.count
    _(subject.collection.map(&:object)).must_equal result
  end

  private

  def subject
    @subject ||= CharactersIndex.call(user: @users.first, search_form: @search_form_mock)
  end

  def gen_random_characters
    @users.map { |u| create_list(:character, 3, author: u) }
          .flatten
  end

  NAMES = %w[Mike Mikki Dorothy Kane].freeze

  def gen_named_characters
    @users.map do |u|
      NAMES.shuffle.map do |name|
        create(:character, name: "#{name} #{Faker::Name.last_name}", author: u)
      end
    end.flatten
  end
end
