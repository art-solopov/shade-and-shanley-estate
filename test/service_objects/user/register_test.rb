require 'test_helper'

class User::RegisterTest < ActiveSupport::TestCase
  def setup
    @user = create(:user, :with_invite_code)
    @invite_code = user.invite_code
    @uid = SecureRandom.hex(10)
  end

  attr_reader :user

  test 'valid data' do
    assert_equal(:ok, subject.call)
    assert_equal(user, subject.user)
    assert_equal(@uid, subject.user.reload.uid)
    assert_nil(subject.user.invite_code)
  end

  test 'invalid data' do
    @invite_code = 'zzzz'
    assert_equal(:not_invited, subject.call)
    assert_nil(subject.user)
  end

  test 'blocked user' do
    @user.update(blocked: true)
    assert_equal(:not_invited, subject.call)
    assert_nil(subject.user)
  end

  private

  def subject
    @subject ||= User::Register.new({ 'uid' => @uid }, { 'invite_code' => @invite_code })
  end
end
