require "test_helper"

class User::SignInTest < ActiveSupport::TestCase
  def setup
    @user = create(:user, :with_uid)
    @uid = user.uid
    @form_data = { 'login_info_form' => { 'remember_me' => '1' } }
  end

  attr_reader :user

  test 'valid data' do
    _(subject.call).must_equal :ok
    _(subject.user).must_equal user
    _(subject.permanent_token).wont_be_nil
  end

  test 'valid data — without remember me token' do
    @form_data.merge!('login_info_form' => { 'remember_me' => '0' })

    _(subject.call).must_equal :ok
    _(subject.user).must_equal user
    _(subject.permanent_token).must_be_nil
  end

  test 'invalid data' do
    @uid = 'zzzz'
    assert_equal(:not_found, subject.call)
    _(subject.user).must_be_nil
    _(subject.permanent_token).must_be_nil
  end

  test 'blocked user' do
    @user.update(blocked: true)
    assert_equal(:not_found, subject.call)
    _(subject.user).must_be_nil
    _(subject.permanent_token).must_be_nil
  end

  private

  def subject
    @subject ||= User::SignIn.new({ 'uid' => @uid }, @form_data)
  end
end
