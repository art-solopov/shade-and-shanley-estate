require "test_helper"

describe SessionsController do
  before do
    @user = create(:user)
    @permanent_token = create(:permanent_auth_token, user: @user)
  end

  describe 'login form' do
    it do
      get login_path
      _(response).must_be :successful?
    end
  end

  describe 'sign in' do
    before do
      @mock_auth = { uid: '1234' }
      OmniAuth.config.mock_auth[:default] = @mock_auth
      @service_double = MiniTest::Mock.new
      @service_double.expect :call, :ok
      @service_double.expect :user, @user
      @service_double.expect :permanent_token, @permanent_token
    end

    it do
      User::SignIn.stub :new, (proc { @service_double }) do
        post login_callback_path(:default), env: { 'omniauth.auth' => @mock_auth }
        _(session[:user_id]).must_equal @user.id
        _(ad_cookies.signed[:remember_me]).must_equal @permanent_token.token
      end
    end
  end

  describe 'sign out' do
    before do
      login_with @user, remember_me: true
    end

    it do
      delete logout_path
      _(response).must_be :redirect?
      get '/'
      _(ad_cookies[:remember_me]).must_be :blank?
    end
  end

  private

  def ad_cookies
    ActionDispatch::Cookies::CookieJar.build(request, cookies.to_hash)
  end
end
