# frozen_string_literal: true

require 'test_helper'

class Admin::CharactersControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = create(:admin)

    @characters = create_list(:character, 10)
    @character = @characters.first

    login_with @user
  end

  test 'index' do
    get admin_characters_url
    _(response).must_be :successful?
  end

  test 'edit' do
    get edit_admin_character_url(@character)
    _(response).must_be :successful?
  end

  test 'new' do
    get new_admin_character_url
    _(response).must_be :successful?
  end

  test 'create - valid data' do
    post admin_characters_url,
         params: {
           character: {
             name: 'Test', species: 'Human', role: 'Guest',
             bio: 'Lorem ipsum'
           }
         }

    _(response).must_be :redirect?
    _(Character.count).must_equal(@characters.count + 1)
  end

  test 'create - invalid data' do
    post admin_characters_url, params: { character: { name: '' } }
    _(response).must_be :ok?
    _(Character.count).must_equal @characters.count
  end

  test 'update - valid data' do
    patch admin_character_url(@character), params: {
      character: { name: 'Test' }
    }

    _(response).must_be :redirect?
    _(@character.reload.name).must_equal 'Test'
  end

  test 'update - invalid data' do
    patch admin_character_url(@character), params: { character: { name: '' } }

    _(response).must_be :ok?
  end

  test 'destroy' do
    delete admin_character_url(@character)

    _(response).must_be :redirect?
    _(Character.count).must_equal(@characters.count - 1)
  end
end
