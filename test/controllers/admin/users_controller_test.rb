require "test_helper"

describe Admin::UsersController do
  before do
    @admin = create(:admin)
    @users = create_list(:user, 4)
    @users << @admin
    @user = @users.first

    login_with(@admin)
  end

  describe '#index' do
    it do
      get admin_users_url
      _(response).must_be :successful?
    end
  end

  describe '#edit' do
    it do
      get edit_admin_user_url(@user)
      _(response).must_be :successful?
    end
  end

  describe '#new' do
    it do
      get new_admin_user_url
      _(response).must_be :successful?
    end
  end

  describe '#create' do
    it 'valid data' do
      post admin_users_url, params: {
             user: { display_name: 'Ranger', invite_code: 'RX12',
                     user_type: 'estate_user' }
           }
      _(response).must_be :redirect?
      _(User.count).must_equal 6
      _(flash[:notice]).wont_be :blank?
    end

    it 'invalid data' do
      post admin_users_url, params: { user: { user_type: 'estate_user' } }
      _(response).must_be :successful?
      _(flash[:notice]).must_be :blank?
    end
  end

  describe '#update' do
    it 'valid data' do
      patch admin_user_url(@user), params: {
              user: { invite_code: 'qwerty' }
            }
      _(response).must_be :redirect?
      _(flash[:notice]).wont_be :blank?
      _(@user.reload.invite_code).must_equal 'qwerty'
    end

    it 'invalid data' do
      patch admin_user_url(@user), params: {
              user: { display_name: '' }
            }
      _(response).must_be :successful?
    end
  end

  describe '#destroy' do
    it do
      delete admin_user_url(@user)
      _(response).must_be :redirect?
      _(flash[:notice]).wont_be :blank?
      _(User.count).must_equal 4
    end
  end

  describe '#block' do
    it do
      patch block_admin_user_path(@user)
      _(response).must_be :redirect?
      _(flash[:notice]).wont_be :blank?
      _(@user.reload).must_be :blocked?
    end
  end

  describe '#unblock' do
    before { @user.update(blocked: true) }
    it do
      patch unblock_admin_user_path(@user)
      _(response).must_be :redirect?
      _(flash[:notice]).wont_be :blank?
      _(@user.reload).wont_be :blocked?
    end
  end
end
