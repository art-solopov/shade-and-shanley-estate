require "test_helper"

class Admin::HomeControllerTest < ActionDispatch::IntegrationTest
  def setup
    @admin = create(:admin)
    @user = create(:user)
  end

  test 'valid user' do
    login_with(@admin)
    get admin_root_url
    _(response).must_be :ok?
  end

  test 'invalid user' do
    login_with(@user)
    _(proc { get admin_root_url }).must_raise(Pundit::NotAuthorizedError)
  end
end
