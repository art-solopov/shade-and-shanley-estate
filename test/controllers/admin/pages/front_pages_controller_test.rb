require "test_helper"

class Admin::Pages::FrontPagesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = create(:admin)

    login_with @user
  end

  test 'show' do
    get admin_pages_front_page_url
    _(response).must_be :successful?
  end

  test 'edit' do
    get edit_admin_pages_front_page_url
    _(response).must_be :successful?
  end

  test 'update - valid data' do
    patch admin_pages_front_page_url,
          params: {
            page: { content: "TEST\n\nHello *world*" }
          }
    _(response).must_be :redirect?
    _(Pages::FrontPage.count).must_equal(1)
  end

  test 'update - invalid data' do
    skip 'No invalid data so far'
  end
end
