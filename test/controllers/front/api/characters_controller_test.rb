require "test_helper"

describe Front::Api::CharactersController do
  before do
    @user = create(:user, :admin)
    @characters = [
      create(:character, name: 'Maria Theresa'),
      create(:character, name: 'Mary Marvel'),
      create(:character, name: 'Richard Gear'),
    ]

    login_with @user
  end

  describe '#show' do
    it do
      get api_characters_search_url(search: 'mar')
      assert_equal @characters[0..1].map { |e| [e.id, e.name] }.to_json, response.body
    end
  end
end