# frozen_string_literal: true

require 'test_helper'

describe Front::CharactersController do
  before do
    # Creating characters with random users
    @characters = create_list(:character, 5)
    @user = create(:user, :with_uid)
    @characters += create_list(:character, 5, author: @user)
    @character = @characters.last

    login_with(@user)
  end

  describe '#index' do
    it do
      get characters_url
      _(response).must_be :successful?
    end
  end

  describe '#show' do
    it do
      get character_url(@character)
      _(response).must_be :successful?
    end
  end

  describe '#create' do
    it 'valid data' do
      post characters_url, params: {
        character: { name: 'TEST', species: 'TEST', role: 'test' }
      }
      _(response).must_be :redirect?
      _(Character.last.name).must_equal 'TEST'
    end

    it 'invalid data' do
      post characters_url, params: {
        character: { name: '' }
      }
      _(response).wont_be :redirect?
    end
  end

  describe '#update' do
    it 'valid data' do
      patch character_url(@character), params: {
        character: { name: 'TEST', species: 'TEST', role: 'test' }
      }
      _(response).must_be :redirect?
      _(@character.reload.name).must_equal 'TEST'
    end

    it 'valid data with associations' do
      patch character_url(@character), params: {
        character: {
          name: 'TEST', species: 'TEST', role: 'test',
          left_character_relationships_attributes: {
            '0' => {
              label_left: 'friend', label_right: 'friend',
              character_right_id: @characters.first.id
            }
          },
          right_character_relationships_attributes: {
            '2' => {
              label_left: 'friend', label_right: 'friend',
              character_left_id: @characters.second.id
            }
          }
        }
      }

      _(response).must_be :redirect?
      _(@character.reload.relationships.map.sort_by { |_n, char| char.id })
        .must_equal [['friend', @characters.first],
                     ['friend', @characters.second]]
    end

    it 'invalid data' do
      patch character_url(@character), params: {
        character: { name: '' }
      }
      _(response).wont_be :redirect?
    end
  end

  describe '#destroy' do
    it do
      delete character_url(@character)
      _(response).must_be :redirect?
      _(Character.count).must_equal(@characters.count - 1)
    end
  end
end
