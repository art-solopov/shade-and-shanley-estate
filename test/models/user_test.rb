require "test_helper"

describe User do
  let(:user) { build(:user) }

  describe 'validations' do
    it { value(user).must_be :valid? }

    it 'display_name presence' do
      user.display_name = nil
      value(user).must_be :invalid?
    end

    describe 'invite_code uniqueness' do
      let(:user2) { create(:user, invite_code: 'QVZ') }

      it 'with invite code' do
        user.invite_code = user2.invite_code
        value(user).must_be :invalid?
      end

      it 'without invite code' do
        user2.update(invite_code: '')
        value(user).must_be :valid?
      end
    end
  end
end
