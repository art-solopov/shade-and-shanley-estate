# frozen_string_literal: true

require 'test_helper'

class PermanentAuthTokenTest < ActiveSupport::TestCase
  def permanent_auth_token
    @permanent_auth_token ||= create(:permanent_auth_token)
  end

  def test_valid
    _(permanent_auth_token).must_be :valid?
  end

  def test_token_presence
    permanent_auth_token.token = ''
    _(permanent_auth_token).must_be :invalid?
  end

  def test_expires_at_presence
    permanent_auth_token.expires_at = nil
    _(permanent_auth_token).must_be :invalid?
  end

  class ClassMethodsTest < ActiveSupport::TestCase
    def test_generate
      user = create(:user)
      token = PermanentAuthToken.generate!(user)
      _(PermanentAuthToken.count).must_equal 1
      _(token).must_be_kind_of PermanentAuthToken
    end
  end
end
