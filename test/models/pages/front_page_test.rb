require "test_helper"

class Pages::FrontPageTest < ActiveSupport::TestCase
  def front_page
    @front_page ||= Pages::FrontPage.new
  end

  def test_valid
    assert front_page.valid?
  end
end
