require "test_helper"

describe Character do
  let(:character) { create(:character) }

  describe 'validations' do
    it { value(character).must_be :valid? }

    it 'author presence' do
      character.author = nil
      value(character).must_be :invalid?
    end

    it 'name presence' do
      character.name = nil
      value(character).must_be :invalid?
    end

    it 'role presence' do
      character.role = nil
      value(character).must_be :invalid?
    end
  end

  describe 'callbacks' do
    describe '#capitalize_indexable_fields' do
      it 'upcases the first letter' do
        character.role = 'maid'
        character.save!
        assert_equal 'Maid', character.role
      end

      it "doesn't downcase everything else" do
        character.species = 'Super Mega Ultra Chicken'
        character.save!
        assert_equal 'Super Mega Ultra Chicken', character.species
      end
    end
  end
end
