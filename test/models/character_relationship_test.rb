require "test_helper"

class CharacterRelationshipTest < ActiveSupport::TestCase
  def character_relationship
    @character_relationship ||= create(:character_relationship)
  end

  def test_valid
    assert character_relationship.valid?
  end
end
