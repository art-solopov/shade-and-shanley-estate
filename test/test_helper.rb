ENV["RAILS_ENV"] = "test"
require File.expand_path("../../config/environment", __FILE__)
require "rails/test_help"
require "minitest/rails"

# To add Capybara feature tests add `gem "minitest-rails-capybara"`
# to the test group in the Gemfile and uncomment the following:
# require "minitest/rails/capybara"

# Uncomment for awesome colorful output
# require "minitest/pride"

MiniTest::Reporters.use!

OmniAuth.config.test_mode = true

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all
  # Add more helper methods to be used by all tests here...
  include FactoryBot::Syntax::Methods
end

class ActionDispatch::IntegrationTest
  def login_with(user, remember_me: false)
    OmniAuth.config.add_mock(:default, { uid: user.uid })
    env = { 'omniauth.auth' => { 'uid' => user.uid } }
    env['omniauth.params'] = { 'login_info_form' => { 'remember_me' => '1' } } if remember_me
    post login_callback_path(provider: :default),
         env: env
  end

  def logout
    delete logout_path
  end
end
