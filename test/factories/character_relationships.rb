FactoryBot.define do
  factory :character_relationship do
    association :character_left, factory: :character
    association :character_right, factory: :character
    label_left { "MyString" }
    label_right { "MyString" }
  end
end
