FactoryBot.define do
  factory :user do
    display_name { Faker::Superhero.name }

    trait :with_invite_code do
      sequence(:invite_code) { |i| "#{SecureRandom.hex(8)}#{i}" }
    end

    trait :with_uid do
      sequence(:uid) { |i| "#{SecureRandom.hex(8)}#{i}" }
    end

    trait :admin do
      user_type { :admin }
    end

    factory :admin, traits: [:admin, :with_uid]
  end
end
