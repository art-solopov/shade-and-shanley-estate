FactoryBot.define do
  factory :permanent_auth_token do
    association(:user)
    token { SecureRandom.hex(8) }
    expires_at { Time.current + 1.day }
  end
end
