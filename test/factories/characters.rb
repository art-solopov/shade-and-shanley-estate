FactoryBot.define do
  factory :character do
    association :author, factory: :user
    name { Faker::Name.name }
    species { %w[human elf dwarf dragon felin lupan].sample }
    bio { Faker::Lorem.paragraph }
    role { %w[guest maid pet].sample }
  end
end
