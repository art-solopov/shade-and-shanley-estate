set -e
cp /secrets/secrets.yml.key config/secrets.yml.key
bundle exec rails db:migrate
bundle exec rails assets:precompile
