Rails.application.routes.draw do
  scope module: :front do
    root 'home#show'

    resources :characters

    namespace :api do
      get 'characters_search', action: :search, controller: :characters
    end
  end

  namespace :admin do
    root 'home#show'

    resources :users do
      patch :block, on: :member
      patch :unblock, on: :member
      get :invite_code, on: :collection
    end

    resources :characters, except: [:show]
    namespace :pages do
      resource :front_page, except: [:index, :new, :create, :destroy], path: :front
    end
  end

  get :login, to: 'sessions#new'
  match '/auth/:provider/callback', to: 'sessions#create', as: :login_callback, via: [:get, :post]
  get '/logout', to: 'sessions#destroy'
  delete '/logout', to: 'sessions#destroy'
end
