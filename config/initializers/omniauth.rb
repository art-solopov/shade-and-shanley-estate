Rails.application.config.middleware.use OmniAuth::Builder do
  unless Rails.env.production?
    provider :developer, fields: [:uid], uid_field: :uid
  end

  if Rails.application.secrets[:discord]
    provider :discord, Rails.application.secrets[:discord][:client_id], Rails.application.secrets[:discord][:secret]
  end
end

OmniAuth.config.logger = Rails.logger

OmniAuth.config.full_host = proc do |env|
  host = env['HTTP_HOST']

  uri = URI("http://#{host}")
  uri.scheme = 'https' if Rails.env.production?

  uri.to_s
end
