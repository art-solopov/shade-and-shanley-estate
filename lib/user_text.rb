module UserText
  module_function

  def process_md(md)
    cleanup_html(compile_md(md))
  end

  def compile_md(md)
    md_compiler = Redcarpet::Markdown.new(
      MDRenderer,
      tables: true
    )
    md_compiler.render(md)
  end

  def cleanup_html(html)
    Loofah.scrub_fragment(html, :prune).to_s.html_safe
  end

  class MDRenderer < Redcarpet::Render::HTML
    delegate :url_helpers, to: 'Rails.application.routes'

    def postprocess(doc)
      nk_doc = Nokogiri::HTML.fragment(doc)
      links = nk_doc.css('a[href^="char:"]').map do |e|
        [
          e,
          CGI.unescape(e[:href]).match(%r{char:\s*/*(.*)$})[1].downcase
        ]
      end
      return doc if links.empty?
      chars = Character.where('lower(name) IN (?)',
                              links.map(&:second))
                       .index_by { |ch| ch.name.downcase }
      links.each do |el, char_name|
        char = chars[char_name]
        el[:href] = char ? url_helpers.character_path(char.id) : ''
      end
      nk_doc.to_html
    end
  end
end
