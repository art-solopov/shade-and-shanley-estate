module Bootstrap
  class FormBuilder < ActionView::Helpers::FormBuilder
    BASIC_FIELDS = (field_helpers - [:label, :check_box, :radio_button,
                                     :fields_for, :fields, :hidden_field,
                                     :file_field])
    BASIC_FIELDS.each do |helper|
      define_method(helper) do |method, options = {}|
        cls = element_class(method, options)
        wrapper(method, options) do
          super(method, options.merge(class: cls))
        end
      end
    end

    def select(method, choices, options = {}, html_options = {})
      cls = element_class(method, html_options)
      wrapper(method, html_options) do
        super(method, choices, options, html_options.merge(class: cls))
      end
    end

    def submit(value = nil, **options)
      cls = Array(options.delete(:class)).map { |e| e.split(/\s+/) }.flatten
      type = options.delete(:type)
      if type.present?
        cls.each { |s| s.gsub!(/\bbtn-.*\s/, '') }
        cls << "btn-#{type}"
      end
      cls << 'btn' unless cls.include?('btn')
      super(value, options.merge(class: cls))
    end

    private

    def wrapper(method, options)
      hint_html = options.delete(:hint_html)
      form_group do
        label(method) + yield + hint(hint_html) + error_hint(method)
      end
    end

    def form_group
      @template.content_tag(:div, class: 'form-group') { yield }
    end

    def element_class(method, options)
      Array(options.delete(:class)).tap do |cls|
        cls << 'form-control'
        cls << 'border' << 'border-danger' if object.errors[method].present?
      end
    end

    def hint(html)
      return '' unless html.present?
      form_text(html, class: 'text-muted')
    end

    def error_hint(method)
      err = object.errors[method]
      return '' unless err.present?
      form_text(object.errors[method].join(', '), class: 'text-danger')
    end

    def form_text(html, options)
      cls = Array(options[:class]).tap { |c| c << 'form-text' }.join(' ')
      ("<small class=\"#{cls}\">#{html}</small>").html_safe
    end
  end
end
